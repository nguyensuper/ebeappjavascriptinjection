// if (typeof api_path === "undefined") {
//   var api_path = 'https://bh.bachhoorder.com/api/v1/';
//   var token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMzQxMCIsImlzcyI6Imh0dHBzOlwvXC9iaC5iYWNoaG9vcmRlci5jb21cL2FwaVwvdjFcL2xvZ2luIiwiaWF0IjoxNjEzODkxNzMzLCJleHAiOjM2MDAxNjEzODkxNzMzLCJuYmYiOjE2MTM4OTE3MzMsImp0aSI6IjMzNWVjMDdiZTg0NzlhODI1NWRjZWZmZTQ2Yzk3ODI0In0.3Q9LYWTTJhT5-eFuetxqli1RfVvLatKIdQk9HWXXKL8';
// }


var rules = {
  '1688': {
    'key': '1688',
    'translate': {
      'originPrice': 'tr.price > td.price-title',
      'promoPrice': 'tr.price > td.price-title',
      'size': '.d-content .obj-sku .obj-title,.obj-sku .obj-header span',
      'color': '.d-content .obj-leading .obj-title,.obj-leading .obj-header span',
      'amount': '',
      'unit': '',
    },
    'crawle': {
      'originPrice': '.tm-price-panel .tm-price',
      'promoPrice': '.tm-promo-panel .tm-price',
      'image': '.item-wrapepr .rax-image',
      'shop_nick': 'span.store-trt-title',
      'shop_link': 'div.store-top-right a, a.store-top',
      'amount': '#J_Amount input',
      'size': '.obj-sku .table-sku td.name',
      'color': '.sku-model .sidebar-wrapper div[style*="background: rgb(255, 255, 255)"]',
      'lowPrice': '#J_PromoPrice .tm-price',
      'highPrice': '#J_PromoPrice .tm-price',
      'name': 'span.title-first-text',
    },
    'params': {
      'originPrice': 'innerText',
      'promoPrice': 'innerText',
      'amount': 'innerText',
      'size': 'innerText',
      'image': 'src',
      'color': 'innerText,title',
      'shop_nick': 'innerText',
      'shop_link': 'href|noBlankPlain',
      'name': 'innerText',
    }
  },
  'taobao': {
    'key': 'taobao',
    'name': 'TAOBAO',
    'encoding': 'gbk',
    'sizeGlobal': '[data-pid="5569827"],[data-pid="20518"],[data-pid="20549"],[data-pid="20509"],[data-pid="164656271"],[data-pid="122216343"]',
    'colorGlobal': '[data-pid="1627207"]',
    'translate': {
      'originPrice': '#J_PriceName',
      'promoPrice': '#J_PriceName',
      'size': '[data-pid="5569827"] .modal-sku-content-title,[data-pid="20518"] .modal-sku-content-title,[data-pid="20509"] .modal-sku-content-title,[data-pid="122216343"] .modal-sku-content-title',
      'color': '[data-pid="1627207"] .modal-sku-content-title',
      'type': '[data-pid="-1"] .modal-sku-content-title',
      'amount': '.modal-sku-content-quantity-title',
      'unit': '.tb-amount-widget .mui-amount-unit',
    },
    'crawle': {
      'originPrice': '#J_priceStd .tb-rmb-num, #J_StrPrice,span.item-price',
      'name': '.tb-main-title,h2.title',
      'promoPrice': '#J_PromoPrice .tb-rmb-num, #J_PromoPriceNum',
      'image': '.modal-sku-image img,#J_ThumbView, #J_ImgBooth',
      'slideshow': '#J_UlThumb li div.tb-pic a img,#goods img',
      'shop_nick': 'div.tb-shop-name > dl > dd  a,.shop-title-text',
      'shop_link': 'div.tb-shop-name > dl > dd  a,.shop-link-item',
      'amount': '#J_SpanStock',
      'size': 'dt:contains("Kích thước"), dt:contains("kích thước"), dt:contains("Size"), dt:contains("size"),ul[data-property="尺码"] li a,ul[data-property="尺寸"] li a,ul[data-property="参考身高"] li a,ul[data-property="辅材套餐"] li a',
      'color': 'dt:contains("Màu sắc"), dt:contains("màu sắc"), dt:contains("màu số"), dt:contains("Color"), dt:contains("color"),ul[data-property="颜色分类"] li a',
      'lowPrice': 'span[itemprop="lowPrice"]',
      'highPrice': 'span[itemprop="highPrice"]'
    },
    'params': {
      'originPrice': 'innerText',
      'name': 'innerText',
      'promoPrice': 'innerText',
      'slideshow': 'src',
      'amount': 'innerText',
      'size': 'innerText',
      'color': 'innerText',
      'shop_nick': 'href|innerText',
      'shop_link': 'href',
    },
  },
  'taobaoh5': {
    'key': 'taobaoh5',
    'name': 'TAOBAO',
    'encoding': 'gbk',
    'sizeGlobal': '[data-pid="5569827"],[data-pid="20518"],[data-pid="20549"],[data-pid="20509"],[data-pid="164656271"],[data-pid="122216343"]',
    'colorGlobal': '[data-pid="1627207"]',
    'translate': {
      'originPrice': '#J_PriceName',
      'promoPrice': '#J_PriceName',
      'size': '[data-pid="5569827"] .modal-sku-content-title,[data-pid="20518"] .modal-sku-content-title,[data-pid="20509"] .modal-sku-content-title,[data-pid="122216343"] .modal-sku-content-title',
      'color': '[data-pid="1627207"] .modal-sku-content-title',
      'type': '[data-pid="-1"] .modal-sku-content-title',
      'amount': '.sku-quantity h2',
      'unit': '.tb-amount-widget .mui-amount-unit',
    },
    'crawle': {
      'originPrice': '.sku-pro-info p.h',
      'name': '.sku-pro-info .sku-title',
      'promoPrice': '.sku-pro-info p.h',
      'image': '.sku-img img',//
      'slideshow': '.sku-img img',
      'shop_nick': 'div.tb-shop-name > dl > dd  a,.shop-title-text',
      'shop_link': 'div.tb-shop-name > dl > dd  a,.shop-link-item',
      'amount': '.sku-quantity p.btn-input input',//
      'size': 'dt:contains("Kích thước"), dt:contains("kích thước"), dt:contains("Size"), dt:contains("size"),ul[data-property="尺码"] li a,ul[data-property="尺寸"] li a,ul[data-property="参考身高"] li a,ul[data-property="辅材套餐"] li a',
      'color': '.sku-txt .c-sku',
      'lowPrice': 'span[itemprop="lowPrice"]',
      'highPrice': 'span[itemprop="highPrice"]'
    },
    'params': {
      'originPrice': 'innerText',
      'name': 'innerText',
      'promoPrice': 'innerText',
      'slideshow': 'src',
      'amount': 'innerText',
      'size': 'innerText',
      'color': 'innerText',
      'shop_nick': 'href|innerText',
      'shop_link': 'href',
    },
  }
};
function removeElTag(str) {
  while (document.querySelectorAll(str).length > 0) {
    var temp_el = document.querySelectorAll(str)[0];
    if (temp_el) temp_el.remove();
  }
}
function translate(key, tran) {
  if (key == '') return false;
  if (key == undefined) return false;
  try {
    if (document.querySelectorAll(key).length > 0) {
      document.querySelectorAll(key)[0].innerText = tran;
    }
  } catch (translate_err) {
    console.log("#69", translate_err);
  }

}
function doTranslate(rule) {
  translate(rule.translate.originPrice, 'Giá');
  translate(rule.translate.promoPrice, 'Giá ưu đãi');
  translate(rule.translate.size, 'Cỡ');
  translate(rule.translate.type, 'Kiểu');
  translate(rule.translate.color, 'Màu sắc');
  translate(rule.translate.amount, 'Số lượng');
  translate(rule.translate.unit, 'Đơn vị');
}
function getParam(rule, key) {
  var selector = rule.crawle[key];
  var attr = rule.params[key];
  var attr_array = [];
  if (typeof (attr) == 'string') {
    attr_array = attr.split('|');
  } else {
    attr_array = ['innerText', 'src'];
  }
  var rt = 'N/A';
  if (document.querySelectorAll(selector).length > 0) {

    var els = document.querySelectorAll(selector);
    for (var i in els) {
      var el = els[i];
      for (var ai in attr_array) {
        var temp_attr = attr_array[ai];
        if (el[temp_attr]) {
          rt = el[temp_attr];
          if (temp_attr == 'src' && rt.startsWith('http')) return rt;
        }
      }
    }
  }
  return rt;
}
function closeCart() {
  document.querySelector('[id^="J_DetailSkuSelector_Mask"]').click();
}
function showToast(msg) {
  console.log("TOAST ", msg);
  if (window.postMessage) window.postMessage(msg);
}
function addToCart(rule) {
  console.log(rule)
  console.log("Adding to cart");
  var currentURL = encodeURIComponent(window.location.href);
  if (window.location.href.indexOf('h5.m.taobao.com') > 0 && document.evaluate('//*[@id="storeCardSection"]/div/div/div/div[3]/div[2]/span', document, null, XPathResult.ANY_TYPE, null).iterateNext()) {
    var shop_nick = document.evaluate('//*[@id="storeCardSection"]/div/div/div/div[3]/div[2]/span', document, null, XPathResult.ANY_TYPE, null).iterateNext().innerHTML
  } else {
    var shop_nick = getParam(rule, 'shop_nick');
  }
  if (window.location.href.indexOf('h5.m.taobao.com') > 0 && document.evaluate('//*[@id="storeCardSection"]/div/div/div/div[3]/div[2]/span', document, null, XPathResult.ANY_TYPE, null).iterateNext()) {
    try {
      var shop_link = "https://shop.m.taobao.com/shop/shop_index.htm?user_id=" + document.querySelector(".img-wrapper img").getAttribute("src").split("/")[5]
    } catch (ex) {
      var shop_link = "N/A"
    }
  } else {
    var shop_link = getParam(rule, 'shop_link');
  }
  var product_name = getParam(rule, 'name');
  var image_src = getParam(rule, 'image');
  var dataCart = [];
  if (shop_nick == 'N/A') {
    if (typeof _iDetailDatConfig != 'undefined') {
      if (_iDetailDataConfig.sellerLoginId) {
        shop_nick = _iDetailDataConfig.sellerLoginId;
        if (shop_link === 'N/A') {
          shop_link = 'https://m.1688.com/winport/' + _iDetailDataConfig.sellerMemberId + '.html';
        }
      }
    }

  }
  if (shop_link === 'N/A') {
    shop_link = window.location.href;
  }
  shop_link = shop_link.split("#")[0]
  shop_link = encodeURIComponent(shop_link);
  var color = 'N/A';
  if (document.querySelectorAll('.image.selected').length > 0) {
    color = document.querySelectorAll('.image.selected')[0].title;
  }
  if (document.querySelectorAll('.active-sku-item').length > 0) {
    color = document.querySelectorAll('.active-sku-item')[0].innerText;
  }
  if (document.querySelectorAll('.sku-model .sidebar-wrapper div[style*="background: rgb(255, 255, 255)"] .text').length > 0) {
    color = document.querySelectorAll('.sku-model .sidebar-wrapper div[style*="background: rgb(255, 255, 255)"] .text')[0].innerText;
  }
  if (document.querySelectorAll('.sku-txt .c-sku').length > 0) {
    color = document.querySelectorAll('.sku-txt .c-sku')[0].innerText;
  }

  if (rule.colorGlobal) {
    var colorGlobalEl = document.querySelector(rule.colorGlobal);
    if (colorGlobalEl) {
      var activeItem = colorGlobalEl.querySelector('.modal-sku-content-item-active');
      if (activeItem) {
        color = activeItem.innerText;
      } else {
        showToast('Bạn chưa chọn màu sắc');
        return false;
      }
    }
  }
  var size = 'N/A';
  if (rule.sizeGlobal) {
    var sizeGlobalEl = document.querySelector(rule.sizeGlobal);
    if (sizeGlobalEl) {
      var activeItem = sizeGlobalEl.querySelector('.modal-sku-content-item-active');
      if (activeItem) {
        size = activeItem.innerText;
      } else {
        showToast('Bạn chưa chọn cỡ');
        return false;
      }
    }
  }

  var quantity = '';
  var totalQuantity = 0;
  var minimumQuantity = 1;
  try {
    minimumQuantity = parseInt(document.querySelector('.obj-amount,.amount .value,.price-beigin-amount').innerText);
  } catch (ex) {
    //showToast("#191 error nha");
  }
  var picked = [];
  var domain = '1688';
  if (window.location.href.indexOf('taobao.com') >= 0) domain = 'taobao';
  if (window.location.href.indexOf('tmall') >= 0) domain = 'tmall';
  console.log("#240 Checking");
  document.querySelectorAll('.table-sku input.amount-input,.m-selector-content input.amount-input,.sku-number-edit, .count-widget-wrapper .sku-item-wrapper .input-number,.sku-quantity p.btn-input input').forEach((inp) => {
    if (inp.value > 0) {
      var this_price = 'N/A';

      if (window.location.href.indexOf('1688.com') > 0) {
        var p = inp.parentElement.parentElement;
        console.log("#244", inp.value, p);

        if (p.querySelector('.discountPrice-price')) {
          try {
            this_price = parseFloat(p.querySelector('.discountPrice-price').innerText.replace(/[^0-9\.]/g, ""));
          } catch (ex) {
            //showToast("#191 error nha");
          }
        }
        if (this_price == 'N/A' && document.querySelector('.price-color')) {
          try {
            this_price = parseFloat(document.querySelector('.price-color').innerText.replace(/[^0-9\.]/g, ""));
          } catch (ex) {
            //showToast("#191 error nha");
          }
        }
        if (p.querySelector('.sku-text-name')) {
          size = p.querySelector('.sku-text-name').innerText;
        }
      } else {
        var priceEl = document.querySelector('.modal-sku-title-price,.sku-pro-info p.h');
        if (priceEl) this_price = parseFloat(priceEl.innerText.match(/([0-9]*\.[0-9]+|[0-9]+)/g)[0]);
        console.log(shop_link, shop_nick, color, product_name);
      }

      try {
        totalQuantity += parseInt(inp.value);
      } catch (qerr) {

      }
      size = encodeURI(size)
      dataCart.push({
        url: currentURL,
        pro_link: currentURL,
        color: color,
        name: product_name,
        size: String(size),
        price: this_price,
        price_arr: [],
        quantity: inp.value,
        sizetxt: size,
        colortxt: color,
        shop_nick: shop_nick,
        shop_link: shop_link,
        link: shop_link,
        amount: inp.value,
        note: '',
        beginAmount: 1,
        domain: domain,
        image: image_src
      });
    }
  });
  console.log("#280 After Checking", totalQuantity, minimumQuantity, dataCart);
  //showToast("#229 "+typeof totalQuantity+"/"+typeof minimumQuantity);
  if (totalQuantity < minimumQuantity) {
    showToast('Bạn cần mua ít nhất ' + minimumQuantity + ' sản phẩm');
    return false;
  }
  //ƯshowToast("Sound good");
  if (dataCart.length == 0) {
    showToast('Bạn chưa chọn sản phẩm nào');
    return false;
  }
  var post_data = "cart=" + JSON.stringify(dataCart);
  var debug_str = post_data;

  document.querySelectorAll('.addCartBTN').forEach(elm => elm.innerText = 'Đang thêm vào giỏ hàng..')
  var xhr = new XMLHttpRequest();
  var api_url = api_path + "add-to-cart";
  if (window.location.href.startsWith('https:')) {
    api_url = api_url.replace('http:', 'https:');
  }
  xhr.open("POST", api_url, true);
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhr.setRequestHeader("Authorization", "Bearer " + token);
  xhr.timeout = 5000;// 5 seconds for timeout
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4 && xhr.status == 200) {
      //document.querySelector('body').innerHTML = xhr.responseText+debug_str;
      try {
        var data = JSON.parse(xhr.responseText);
        var doCartBTN = document.querySelectorAll('.addCartBTN')[0];
        if (data.message) {
          document.querySelectorAll('.addCartBTN').forEach(elm => elm.innerText = data.message)
        } else {
          document.querySelectorAll('.addCartBTN').forEach(elm => elm.innerText = 'Thêm thành công!')
        }
      } catch (err2) {
        console.log(err2);
        showToast("Lỗi #2");
      }

    }
    if (xhr.status == 404) {
      showToast("Không tìm thấy mã khách, vui lòng đăng nhập lại");
    }
  }
  xhr.onerror = function (err) {
    showToast("Lỗi #285 ");
  }
  xhr.ontimeout = function () {
    showToast("xhr timeout");
  }
  xhr.send(post_data);
}
function editPage(rule) {
  console.log("=======EDIT PAGE=====");
  console.log("TRANSLATING");
  doTranslate(rule);
  console.log("TRANSLATED");
  var edited = false
  var orderItv1688 = setInterval(() => {
    var checkLoop = 0
    do {
      checkLoop++
      var rowBuyNow = document.querySelector("[data-track=\"order\"] .action-name:not(.modified)")
      if (!rowBuyNow) {
        var rowBuyNow = document.evaluate('//div[text()="立即订购"]', document, null, XPathResult.ANY_TYPE, null).iterateNext()
      }
      if (rowBuyNow && rowBuyNow.innerText.trim() !== "Đặt hàng") {
        rowBuyNow.innerHTML = "Đặt hàng"
        rowBuyNow.classList.add("modified")
        if (!edited) {
          rowBuyNow.click()
        }
        edited = true
      }
      console.log("runed")
    } while (
      (document.querySelector("[data-track=\"order\"] .action-name:not(.modified)") ||
        document.evaluate('//div[text()="立即订购"]', document, null, XPathResult.ANY_TYPE, null).iterateNext()) && checkLoop < 10
    )

    var checkLoopModal = 0
    do {
      checkLoopModal++
      var addToCartBtn = document.querySelector(".order-button:not(.modified)")
      if (!addToCartBtn) {
        addToCartBtn = document.evaluate('//div[text()="确定"]', document, null, XPathResult.ANY_TYPE, null).iterateNext()
      }
      if (addToCartBtn && !addToCartBtn.querySelector(".addCartBTN")) {
        doTranslate(rule);
        addToCartBtn.style.display = 'none';
        addToCartBtn.classList.add("modified")
        addToCartBtn.innerHTML = "Thêm giỏ hàng"
        var addCartBtn = document.createElement("a");
        var t2 = document.createTextNode("Thêm vào giỏ hàng");
        addCartBtn.appendChild(t2);
        addCartBtn.setAttribute('href', 'javascript:void(0);');
        addCartBtn.className = "order-button modified addCartBTN";
        addCartBtn.style["border"] = "0px solid black";
        addCartBtn.style["box-sizing"] = "border-box";
        addCartBtn.style["display"] = "flex";
        addCartBtn.style["flex-direction"] = "column";
        addCartBtn.style["align-content"] = "flex-start";
        addCartBtn.style["flex-shrink"] = "0";
        addCartBtn.style.background = "linear-gradient(90deg, rgb(255, 115, 0) 0%, rgb(255, 96, 0) 100%)";
        addToCartBtn.parentNode.appendChild(addCartBtn);
        addCartBtn.addEventListener("click", () => {
          addToCart(rules[rule.key]);
        });
      }
    } while (
      (document.querySelector(".order-button:not(.modified)") ||
        document.evaluate('//div[text()="确定"]', document, null, XPathResult.ANY_TYPE, null).iterateNext()) && checkLoopModal < 10
    )
  }, 500);


  //auto buy taobao
  console.log("Autobuy taobao");
  var getElementsByXPath = function (xpath, parent) {
    let results = [];
    let query = document.evaluate(xpath, parent || document,
      null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
    // XPathResult.ANY_TYPE
    for (let i = 0, length = query.snapshotLength; i < length; ++i) {
      results.push(query.snapshotItem(i));
    }
    return results;
  }
  var btnList = [
    '//*[text()="立即购买"]',
    '//*[text()="马上抢"]',
    '//*[text()="领券购买"]',
  ]
  //nut o ngoai modal
  var replaceAddToCart = function (rowBuyNow) {
    if (typeof doTranslate === "function") {
      doTranslate(rule);
    }
    if (!rowBuyNow) {
      return false
    }
    if (rowBuyNow.closest(".modal") && !rowBuyNow.closest(".modal-show")) {
      return false
    }
    var addCartBtn = rowBuyNow.cloneNode(true)
    rowBuyNow.innerHTML = "Thêm giỏ hàng"
    addCartBtn.innerHTML = "Thêm vào giỏ hàng"
    addCartBtn.className = addCartBtn.className + " addCartBTN";
    addCartBtn.addEventListener("click", () => {
      addToCart(rules[rule.key]);
    });
    rowBuyNow.parentNode.style.display = "block";
    rowBuyNow.parentNode.childNodes.forEach(elm => elm.style.display = 'none')
    rowBuyNow.parentNode.appendChild(addCartBtn)
  }
  var replaceBuyNow = function (rowBuyNow) {
    rowBuyNow.parentNode.childNodes.forEach(elm => rowBuyNow !== elm ? elm.style.display = 'none' : "")
    if (rowBuyNow.className.includes("ban-sale")) {
      rowBuyNow.innerHTML = "Hết hàng"
    } else {
      rowBuyNow.innerHTML = "Đặt hàng"
      rowBuyNow.click();
    }
  }
  setInterval(() => {
    btnList.forEach(btn => {
      var rowBuyNows = getElementsByXPath(btn)
      rowBuyNows.forEach(rowBuyNow => {
        if (rowBuyNow.closest(".modal") || rowBuyNow.closest(".mt-modal") || rowBuyNow.closest(".dialog")) {
          replaceAddToCart(rowBuyNow);
        } else {
          replaceBuyNow(rowBuyNow)
        }

      })
    })
  }, 500);
}

(function () {

  removeElTag('.smartbanner-wrapper');
  removeElTag('.toolbar__cart');
  document.querySelector("body").childNodes.forEach(elm => {
    if (elm.style && elm.style["z-index"] && parseInt(elm.style["z-index"]) >= 1000000) {
      elm.remove()
    }
  })
  if (window.location.href.startsWith('https://cui.m.1688.com')) {
    var new_src = document.getElementsByTagName('iframe')[0].getAttribute('src');
    if (new_src.startsWith('//')) new_src = 'https:' + new_src;
    window.location.href = new_src;
  }
  if (window.location.href.indexOf('1688.com') > 0) {
    editPage(rules['1688']);
  }
  if (window.location.href.indexOf('m.intl.taobao.com') > 0) {
    editPage(rules['taobao']);
  }
  if (window.location.href.indexOf('h5.m.taobao.com') > 0) {
    editPage(rules['taobaoh5']);
  }



  translate('.obj-leading .obj-header span', 'Màu sắc');
  translate('.obj-sku .obj-header span', 'Kích cỡ');
  console.log("======EDIT DONE===========");
})();